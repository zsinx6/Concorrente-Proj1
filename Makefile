all:
	@gcc semThread.c -o sem -lm && gcc comThread.c -o com -lpthread -lm
clean:
	@rm sem com *.txt
excom250:
	@./com < matrizes/matriz250.txt
excom500:
	@./com < matrizes/matriz500.txt
excom1000:
	@./com < matrizes/matriz1000.txt
excom1500:
	@./com < matrizes/matriz1500.txt
excom2000:
	@./com < matrizes/matriz2000.txt
excom3000:
	@./com < matrizes/matriz3000.txt
excom4000:
	@./com < matrizes/matriz4000.txt
exsem250:
	@./sem < matrizes/matriz250.txt
exsem500:
	@./sem < matrizes/matriz500.txt
exsem1000:
	@./sem < matrizes/matriz1000.txt
exsem1500:
	@./sem < matrizes/matriz1500.txt
exsem2000:
	@./sem < matrizes/matriz2000.txt
exsem3000:
	@./sem < matrizes/matriz3000.txt
exsem4000:
	@./sem < matrizes/matriz4000.txt
