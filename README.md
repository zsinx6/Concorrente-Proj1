Projeto 1 - Programação Concorrente - ICMC - USP

Uso de Pthreads para a resolução de sistemas lineares
Para executar o arquivo deve-se usar o Makefile:
Para compilar: make
Para limpar os executáveis: make clean
Para executar: make ex<com/sem><tamanho>
tamanho: {250,500,1000,1500,2000,3000,4000}

Exemplo de execução: make exsem500
executa o programa sem threads para a matriz de ordem 500

Créditos:
Lucas de Carvalho Rodrigues da Silva
Paula Silveira Barbosa