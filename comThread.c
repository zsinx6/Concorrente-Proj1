//Codigo sequencial sem o uso de threads
//Lucas de Carvalho Rodrigues da Silva, 8624511
//Paula Silveira Barbosa, 7149412

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>

//Numero de threads
#define NUM 4

double **MA, *MB, *x, *new_x, J_ERROR, *diff, mr, oldMB, *oldMA, test;
int nt, J_ORDER, J_ROW_TEST, J_ITE_MAX;
int terminate;
pthread_barrier_t barrier1, barrier2;
struct timeval tv1, tv2;

//struct, s representa a primeira linha
//e eh a ultima
typedef struct{
    int s,e;
}thread_st;

//Funcao que sera passada para a thread
//Onde as iteracoes ocorrerao
void *Richardson(void *ptr){
    int i, j;
    int s, e;
    double sum;
    pthread_t id = pthread_self();
    thread_st *data = (thread_st *) ptr;
    s = data->s;
    e = data->e;
    while(terminate){
        for(i=s; i<=e; i++){
            new_x[i] = MB[i];
            sum = 0;
            for(j=0; j<J_ORDER; j++){
                sum += (MA[i][j]*x[j]);
            }
            new_x[i] = new_x[i] - sum;
        }
        //primeira barreira, ao chegar aqui, espera todas as outras
        pthread_barrier_wait(&barrier1);
        //ao chegar na segunda barreira, espera pela main terminar
        //de calcular o erro para saber se deve continuar ou parar
        //usando a variavel "terminate" para decidir
        pthread_barrier_wait(&barrier2);
    }
    pthread_exit(NULL);
}

//Faz a divisao de todos os elementos pela diagonal principal
//e deixa a mesma nula:
void diagAB(){
    int i, j;
    int *aux = malloc(J_ORDER*sizeof(double));
    for(i=0; i<J_ORDER; i++){
        aux[i] = MA[i][i];
    }

    for(i=0; i<J_ORDER; i++){
        for(j=0; j<J_ORDER; j++){
            MA[i][j] = MA[i][j]/aux[i];
        }
        MB[i] = MB[i]/aux[i];
    }
    free(aux);

    for(i=0; i<J_ORDER; i++){
        MA[i][i] = 0;
    }
}

//Funcao que copia a matriz:
void copyMat(){
    int i;
    for(i=0; i<J_ORDER; i++){
        x[i] = new_x[i];
    }
}

//Funcao que retorna o maior valor:
double max(double *v){
    int i;
    double ret;
    ret = fabs(v[0]);
    for(i=1; i<J_ORDER; i++){
        if(ret < fabs(v[i]))
            ret = fabs(v[i]);
    }
    return ret;
}

//Funcoes de debug:
void printMatA(){
    int i, j;
    printf("\n");
    for(i=0; i<J_ORDER; i++){
        for(j=0; j<J_ORDER; j++){
            printf("%lf ", MA[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
void printMatX(){
    int i;
    for(i=0; i<J_ORDER; i++){
        printf("%lf ", x[i]);
    }
    printf("\n");
}
void printMatB(){
    int i;
    for(i=0; i<J_ORDER; i++){
        printf("%lf\n", MB[i]);
    }
}
//Fim de funcoes de debug

int main(){
    int i, j;
    float div;
    int prev;
    //MA -> matriz A
    //MB -> matriz B
    //J_ORDER -> ordem da matriz
    //J_ROW_TEST -> linha a ser realizada o teste
    //J_ITE_MAX -> numero maximo de iteracores

    mr = 0;
    nt = 1;
    prev = 0;
    terminate = 1;
    pthread_t threads[NUM];
    thread_st t_data[NUM];

    //Leitura das entradas:
    scanf("%d", &J_ORDER);
    scanf("%d", &J_ROW_TEST);
    scanf("%lf", &J_ERROR);
    scanf("%d", &J_ITE_MAX);

    //Criacao das matrizes:
    MA = (double**)malloc(J_ORDER*sizeof(double*));
    MB = (double*)malloc(J_ORDER*sizeof(double));
    oldMA = (double*)malloc(J_ORDER*sizeof(double));
    x = (double*)malloc(J_ORDER*sizeof(double));
    new_x = (double*)malloc(J_ORDER*sizeof(double));
    diff = (double*)malloc(J_ORDER*sizeof(double));
    memset(diff, 0, J_ORDER);

    //Leitura das matrizes:
    for(i=0; i<J_ORDER; i++){
        MA[i] = (double*)malloc(J_ORDER*sizeof(double));
        for(j=0; j<J_ORDER; j++){
            scanf("%lf", &MA[i][j]);
        }
    }
    for(i=0; i<J_ORDER; i++){
        scanf("%lf", &MB[i]);
    }
    //Fim da leitura das matrizes
    for(i=0; i<J_ORDER; i++){
        oldMA[i] = MA[J_ROW_TEST][i];
    }
    oldMB = MB[J_ROW_TEST];
    gettimeofday(&tv1, NULL);
    //Inicio do algoritmo:
    diagAB();
    for(i=0; i<J_ORDER; i++){
        x[i] = MB[i];
    }
    //barreiras sao criadas
    pthread_barrier_init(&barrier1, NULL, NUM+1);
    pthread_barrier_init(&barrier2, NULL, NUM+1);

    //eh feita a divisao para saber quantas linhas
    //e quais linhas serao de qual thread
    div = J_ORDER/NUM;
    for(i=0; i<NUM; i++){
        t_data[i].s = prev;
        t_data[i].e = prev+div-1;
        prev += div;
        if(i == NUM - 1)
            t_data[i].e = J_ORDER-1;
    }
    //apois decidir as tarefas, as threads sao criadas e executadas
    for(i=0; i<NUM; i++){
        pthread_create(&threads[i], NULL, (void*) &Richardson, (void*) &t_data[i]);
    }
    while(terminate){
        //primeira barreira, que segura a main nesse ponto
        //ate que seja possivel calcular os erros
        pthread_barrier_wait(&barrier1);
        for(i=0; i<J_ORDER; i++){
            diff[i] = fabs(new_x[i]-x[i]);
        }
        mr = max(diff)/max(new_x);
        if(mr <= J_ERROR){
            terminate = 0;
        }
        copyMat();
        if(terminate)
            nt++;
        if(nt > J_ITE_MAX){
            terminate = 0;
        }
        //apos esse ponto, faz com que as threads voltem a executar
        pthread_barrier_wait(&barrier2);
    }
    for(i = 0; i<NUM; i++)
        pthread_join(threads[i], NULL);
    //Fim do algoritmo
    printf("%d\n", nt);
    for(i=0; i<J_ORDER; i++){
        test += oldMA[i]*new_x[i];
    }
    printf("RowTest: %d => [%lf] =? %lf\n", J_ROW_TEST, test, oldMB);
    gettimeofday(&tv2, NULL);
    printf ("Total time = %f seconds\n",
            (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
            (double) (tv2.tv_sec - tv1.tv_sec));
    //Faz a desalocacao de memoria:
    pthread_barrier_destroy(&barrier1);
    pthread_barrier_destroy(&barrier2);
    for(i=0; i<J_ORDER; i++){
        free(MA[i]);
    }
    free(oldMA);
    free(MA);
    free(MB);
    free(x);
    free(new_x);
    free(diff);

    return 0;
}
