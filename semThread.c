//Codigo sequencial sem o uso de threads
//Lucas de Carvalho Rodrigues da Silva, 8624511
//Paula Silveira Barbosa, 7149412
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

double **MA, *MB, *x, *new_x, J_ERROR, *diff, sum, mr, test, oldMB, *oldMA;
int nt, J_ORDER, J_ROW_TEST, J_ITE_MAX;
struct timeval tv1, tv2;

//Faz a divisao de todos os elementos pela diagonal principal
//e deixa a mesma nula:
void diagAB(){
    int i, j;
    int *aux = malloc(J_ORDER*sizeof(double));
    for(i=0; i<J_ORDER; i++){
        aux[i] = MA[i][i];
    }

    for(i=0; i<J_ORDER; i++){
        for(j=0; j<J_ORDER; j++){
            MA[i][j] = MA[i][j]/aux[i];
        }
        MB[i] = MB[i]/aux[i];
    }
    free(aux);

    for(i=0; i<J_ORDER; i++){
        MA[i][i] = 0;
    }
}

//Funcao que copia a matriz:
void copyMat(){
    int i, j;
    for(i=0; i<J_ORDER; i++){
        x[i] = new_x[i];
    }
}

//Funcao que retorna o maior valor:
double max(double *v){
    int i, j;
    double ret;
    ret = fabs(v[0]);
    for(i=1; i<J_ORDER; i++){
        if(ret < fabs(v[i]))
            ret = fabs(v[i]);
    }
    return ret;
}

//Funcoes de debug:
void printMatA(){
    int i, j;
    printf("\n");
    for(i=0; i<J_ORDER; i++){
        for(j=0; j<J_ORDER; j++){
            printf("%lf ", MA[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
void printMatX(){
    int i, j;
    for(i=0; i<J_ORDER; i++){
        printf("%lf ", x[i]);
    }
    printf("\n");
}
void printMatB(){
    int i, j;
    for(i=0; i<J_ORDER; i++){
        printf("%lf\n", MB[i]);
    }
}
//Fim de funcoes de debug

int main(){
    int i, j;
    //MA -> matriz A
    //MB -> matriz B
    //J_ORDER -> ordem da matriz
    //J_ROW_TEST -> linha a ser realizada o teste
    //J_ITE_MAX -> numero maximo de iteracores

    mr = 0;
    nt = 1;
    test = 0;

    //Leitura das entradas:
    scanf("%d", &J_ORDER);
    scanf("%d", &J_ROW_TEST);
    scanf("%lf", &J_ERROR);
    scanf("%d", &J_ITE_MAX);

    //Criacao das matrizes:
    MA = (double**)malloc(J_ORDER*sizeof(double*));
    MB = (double*)malloc(J_ORDER*sizeof(double));
    oldMA = (double*)malloc(J_ORDER*sizeof(double));
    x = (double*)malloc(J_ORDER*sizeof(double));
    new_x = (double*)malloc(J_ORDER*sizeof(double));
    diff = (double*)malloc(J_ORDER*sizeof(double));
    memset(diff, 0, J_ORDER);

    //Leitura das matrizes:
    for(i=0; i<J_ORDER; i++){
        MA[i] = (double*)malloc(J_ORDER*sizeof(double));
        for(j=0; j<J_ORDER; j++){
            scanf("%lf", &MA[i][j]);
        }
    }
    for(i=0; i<J_ORDER; i++){
        scanf("%lf", &MB[i]);
    }
    //Fim da leitura das matrizes

    for(i=0; i<J_ORDER; i++){
        oldMA[i] = MA[J_ROW_TEST][i];
    }
    oldMB = MB[J_ROW_TEST];
    gettimeofday(&tv1, NULL);
    //Inicio do algoritmo:
    diagAB();
    for(i=0; i<J_ORDER; i++){
        x[i] = MB[i];
    }
    while(1){
        if(nt >= J_ITE_MAX)
            break;
        for(i=0; i<J_ORDER; i++){
            new_x[i] = MB[i];
            sum = 0;
            for(j=0; j<J_ORDER; j++){
                sum += (MA[i][j]*x[j]);
            }
            new_x[i] = new_x[i] - sum;
        }
        for(i=0; i<J_ORDER; i++){
            diff[i] = fabs(new_x[i]-x[i]);
        }
        mr = max(diff)/max(new_x);
        if(mr <= J_ERROR)
            break;
        copyMat();
        nt++;
    }
    //Fim do algoritmo
    printf("%d\n", nt);
    for(i=0; i<J_ORDER; i++){
        test += oldMA[i]*new_x[i];
    }
    printf("RowTest: %d => [%lf] =? %lf\n", J_ROW_TEST, test, oldMB);
    gettimeofday(&tv2, NULL);
    printf ("Total time = %f seconds\n",
            (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
            (double) (tv2.tv_sec - tv1.tv_sec));
    //Faz a desalocacao de memoria:
    for(i=0; i<J_ORDER; i++){
        free(MA[i]);
    }
    free(oldMA);
    free(MA);
    free(MB);
    free(x);
    free(new_x);
    free(diff);

    return 0;
}
